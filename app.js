"use strict";

let userNumber1;
let userNumber2;

do {
  userNumber1 = prompt("Введіть число");
  userNumber2 = prompt("Введіть число");
} while (isNaN(userNumber1) || isNaN(userNumber2));

userNumber1 = parseInt(userNumber1);
userNumber2 = parseInt(userNumber2);
console.log("Ви ввели числа: ", userNumber1, userNumber2);

for (let i = userNumber1; i <= userNumber2; i++) {
  if (Number.isInteger(i)) {
    console.log(i);
  }
}

let user = prompt("Введіть число");

if (user % 2 === 0) {
  console.log("Введене вами число є парним!");
} else {
  alert("Введіть будь ласка ще раз");
}
